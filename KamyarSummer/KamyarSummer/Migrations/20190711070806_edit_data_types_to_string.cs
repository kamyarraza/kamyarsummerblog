﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KamyarSummer.Migrations
{
    public partial class edit_data_types_to_string : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "url",
                table: "Gallery",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "title",
                table: "Gallery",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "describtion",
                table: "Gallery",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "url",
                table: "Gallery",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "title",
                table: "Gallery",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "describtion",
                table: "Gallery",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
