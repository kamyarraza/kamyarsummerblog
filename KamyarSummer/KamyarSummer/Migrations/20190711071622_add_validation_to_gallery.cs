﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KamyarSummer.Migrations
{
    public partial class add_validation_to_gallery : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "title",
                table: "Gallery",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "describtion",
                table: "Gallery",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ddtime",
                table: "Gallery",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ddtime",
                table: "Gallery");

            migrationBuilder.AlterColumn<string>(
                name: "title",
                table: "Gallery",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "describtion",
                table: "Gallery",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
