﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KamyarSummer.Models
{
    public class Gallery
    {
        public int ID { get; set; }

        [StringLength(50, MinimumLength = 3)]
        [Required]
        public string title { get; set; }

        [Required]
        public string describtion { get; set; }

        [Display(Name = "ReleaseDate")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime ddtime { get; set; }
        public string url { get; set; }
    }
}
