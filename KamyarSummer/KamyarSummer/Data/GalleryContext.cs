﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace KamyarSummer.Models
{
    public class GalleryContext : DbContext
    {
        public GalleryContext (DbContextOptions<GalleryContext> options)
            : base(options)
        {
        }

        public DbSet<KamyarSummer.Models.Gallery> Gallery { get; set; }
    }
}
